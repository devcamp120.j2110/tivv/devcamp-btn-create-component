import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'

class Display extends Component {

    onBtnExistClick = () => {
        this.props.display(false)
    }
    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');

    }

    shouldComponentUpdate() {
        console.log('Should component update');
        return true;
    }

    componentWillUpdate() {
        console.log('Component Will update');
    }

    componentDidUpdate() {
        console.log("Component Did Update 1");

    }

    componentWillUnmount() {
        console.log("Component will unmount");
    }
    render() {
        console.log("Render Display")
        return (
            <div className="bg-light w-25 text-center">
                <button className="btn btn-info" onClick={this.onBtnExistClick}>Destroy Component</button>
                <h1>I exist!</h1>
            </div>
        )
    }
}
export default Display;