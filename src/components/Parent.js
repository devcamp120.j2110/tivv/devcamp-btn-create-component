import { Component } from "react";
import Display from "./Display";
import 'bootstrap/dist/css/bootstrap.min.css'

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childDisplay: false
        }
    }
    updateDisplay = (value) => {
        this.setState({
            childDisplay: value
        })
    }
    onBtnCreateComponentClick = () => {
        this.setState({
            childDisplay:true
        })
    }
    render() {
        return (
            <div>
                {this.state.childDisplay ? <Display display={this.updateDisplay} />  : <button className="btn btn-success"
                 onClick={this.onBtnCreateComponentClick} >Create component</button> }
            </div>
        )
    }
}
export default Parent;